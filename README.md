# README #

This repository contains some examples on how to:
* convert a csv file (stats_to_xml.csv) to an XML file - described in method convertFromCSVtoXML();
* load model classes information from an XML file - this is demonstrated in method ensureConvertFromXmlFileWorks()

## Release Notes ##

**NOTE 1:** This is just an example/reference project and you **MUST** adapt your own project according to your needs.

**NOTE 2:** This project may containt more or less properties than asked for in LAPR2 Project Assignment and some data might me incorrect.

How to run code quality analysis:

```
$ test jacoco:report org.pitest:pitest-maven:mutationCoverage -DhistoryInputFile=./target/fasterPitMutationTesting-history.txt -DhistoryOutputFile=./target/fasterPitMutationTesting-history.txt -DtimeoutFactor=1.10
```

## Domain Invariants example for division

* Division by 0 is impossible
  * should it throw an IllegalArgumentException or an ArithmeticException?

* Zero divided by something is always 0

* Division by itself is always 1
  * maybe for performance reasons
  
* Number divided by 2 is always even
  * if the developer is developing the division algorithm instead of using Java's native division

