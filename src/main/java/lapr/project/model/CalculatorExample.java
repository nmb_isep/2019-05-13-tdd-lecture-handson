package lapr.project.model;

/**
 * @author Nuno Bettencourt <nmb@isep.ipp.pt> on 24/05/16.
 */
public class CalculatorExample {

    /**
     * Calculate the sum of two int numbers.
     *
     * @param firstOperand  First number to be added
     * @param secondOperand Second number to be added
     * @return Return the sum of both operands.
     */
    public int sum(int firstOperand, int secondOperand) {
        return firstOperand + secondOperand;
    }

    public int divide(int dividend, int divisor) {
        if (divisor == 0) {
            throw new IllegalArgumentException("Divisor can not be 0");
        }
        return dividend / divisor;
    }

    public int factorial(int v) throws IllegalArgumentException {
        if (v < 0) {
            throw new IllegalArgumentException();
        }

        if (v == 0) {
            return 1;
        }

        if (v == 1) {
            return 1;
        }

        return v * factorial(v - 1);
    }

}
