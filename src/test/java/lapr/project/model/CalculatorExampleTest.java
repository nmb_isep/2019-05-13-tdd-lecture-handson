package lapr.project.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * CalculatorExample Class Unit Testing.
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt> on 24/05/16.
 */
public class CalculatorExampleTest {
    /**
     * Ensure second operand can assume a negative value.
     */
    @Test
    public void ensureSumSecondNegativeOperandWorks() {
        //Arrange
        int expectedResult = 5;
        int firstOperand = 10;
        int secondOperand = -5;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.sum(firstOperand, secondOperand);

        //Assert
        assertEquals(expectedResult, result);
    }


    @Test
    public void ensureFatorialOfMinusOneFails() {
        //Arrange
        int input = -1;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            calculator.factorial(input);
        });
    }

    @Test
    public void ensureFatorialOfZeroIsOne() {
        //Arrange
        int input = 0;
        int expectedResult = 1;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.factorial(input);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureFatorialOfTwoIsTwo() {
        //Arrange
        int input = 2;
        int expectedResult = 2;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.factorial(input);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void ensureFatorialOfOneIsOne() {
        //Arrange
        int input = 1;
        int expectedResult = 1;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.factorial(input);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test

    public void ensureFatorialOfFourIsTwentyFour() {
        //Arrange
        int input = 4;
        int expectedResult = 24;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.factorial(input);

        //Assert
        assertEquals(expectedResult, result);
    }


    @Test
    public void ensureZeroDividedBySomethingIsZero() {
        //Arrange

        int dividend = 0;
        int divisor = 5;
        int expectedResult = 0;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.divide(dividend, divisor);

        //Assert
        assertEquals(expectedResult, result);

    }

    @Test
    public void ensureFiveDividedByFiveIsOne() {
        //Arrange

        int dividend = 5;
        int divisor = 5;
        int expectedResult = 1;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        int result = calculator.divide(dividend, divisor);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test

    public void ensureFiveDividedByZeroThrowsIsOne() {
        //Arrange
        int dividend = 5;
        int divisor = 0;
        CalculatorExample calculator = new CalculatorExample();

        //Act
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(dividend, divisor);
        });
    }

}
